import numpy as np
import cv2

# Function to pad the data to image croping when blurring
def padder(variable,pad):
    variable = np.pad(variable,pad,'edge')
    
    return (variable)

# Function to grab the extension for the file being used
def file_type(Filename):
    for i in range(1,len(Filename)+1):
        if(Filename[-i] == "."):
            file_type = Filename[-i+1:]
    
    return file_type

# Funtion to display the image that is passed though with opencv
def display_image(name,image):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.imshow(name,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# Read in an image utilizing opencv and putting it in a variable
def import_image(image_location):
    I_GS = cv2.imread(image_location,0)

    # Display the image after it is read in
    #display_image("Normal",I_GS)

    # Cast the image to float values in order for the calculations to be done
    I_GS = I_GS.astype(np.float32)
    
    return (I_GS)

# Read in a data file matrix utilizing numpy
def import_data(file_location):
    I_GS = np.loadtxt(file_location)

    # Display the image after it is read in
    #display_image("Normal",I_GS)

    # Cast the image to float values in order for the calculations to be done
    I_GS = I_GS.astype(np.float32)
    
    return (I_GS)

# Write out the image to a file utilizing opencv
def write_image(name,variable):
    # Display the final image before it is written
    #display_image(name,variable)
    cv2.imwrite(name+'.png',variable)

# Generate gaussian distribution box
def gaussian(size):
    listing = np.zeros(size)
    for i in range(size):
        if(i == 0):
            listing[0] = 1
        elif(i < size/2):
            listing[i] = listing[i-1]*2
        else:
            listing[i] = listing[i-1]/2
  
    a = np.array([listing])
    b = np.matmul(np.transpose(a),a)
    b = b.astype(np.float32)
    return(b)

def hist_lines(im, amount_bins):
    accumulate = False
    histSize = amount_bins
    histRange = (0,256)
    color = (0,255,0)
    
    hist_w = 512
    hist_h = 400
    bin_w = int(np.around( hist_w/histSize ))

    h = np.zeros((hist_h, hist_w, 3), dtype=np.uint8)

    hist_item = cv2.calcHist([im],[0],None,[histSize],histRange, accumulate=accumulate)
    cv2.normalize(hist_item,hist_item, alpha=0, beta=hist_h, norm_type=cv2.NORM_MINMAX)
    hist=np.int32(np.around(hist_item))

    for i in range(1, histSize):
        cv2.line(h, ( bin_w*(i-1), hist_h - hist[i-1] ),
            ( bin_w*(i), hist_h - hist[i] ),
            color, thickness=2)
    y = h
    return y


def hist_save(im, i, blur_type, amount_bins):
    lines = hist_lines(im, amount_bins)
    write_image("Photos/{}Histogram{}".format(blur_type, i),lines)

def hist_display(im, amount_bins):
    print(''' Histogram plotting \n
    Keymap :\n
    a - show histogram in bin mode \n
    b - show equalized histogram (always in bin mode) \n
    Esc - exit \n
    ''')
    
    cv2.imshow('image',im)
    while True:
        k = cv2.waitKey(0)
        if k == ord('a'):
            print('a')
            lines = hist_lines(im, amount_bins)
            cv2.imshow('histogram',lines)
            cv2.imshow('image',im)
        elif k == ord('b'):
            print('b')
            equ = cv2.equalizeHist(im)
            lines = hist_lines(equ, amount_bins)
            cv2.imshow('histogram',lines)
            cv2.imshow('image',equ)
        elif k == 27:
            print('ESC')
            cv2.destroyAllWindows()
            break
    cv2.destroyAllWindows()

def noise_generator(I_GS, histogram_flag, amount_bins):
    amount_masks = 10
    max_noise = 26

    rows=np.shape(I_GS)[0]
    cols=np.shape(I_GS)[1]

    I_GS = I_GS.ravel().astype(np.uint8)

    uniform_noise = np.zeros((amount_masks,rows*cols), dtype=np.int8)
    gaussian_noise = np.zeros((amount_masks,rows*cols), dtype=np.int8)
    noisy_image = np.zeros((rows*cols), dtype=np.uint8)

    for i in range(amount_masks):
        np.random.seed()
        uniform_noise[i] = (np.random.rand(rows*cols) * (max_noise+1)).astype(np.int8)
        gaussian_noise[i] = np.clip(np.random.normal(loc=max_noise//2, scale=max_noise//6,
                        size=(rows * cols,)), 0, max_noise).astype(np.int8)

    for i in range(amount_masks):
        noisy_image = np.add(I_GS, uniform_noise[i])
        noisy_image = np.clip(noisy_image,0,255).astype(np.uint8)
        write_image('Photos/Uniform{}'.format(i),noisy_image.reshape(rows,cols))
        hist_save(noisy_image.reshape(rows,cols), i, "Uniform", amount_bins)

    for i in range(amount_masks):
        noisy_image = np.add(I_GS, gaussian_noise[i])
        noisy_image = np.clip(noisy_image,0,255).astype(np.uint8)
        write_image('Photos/Gaussian{}'.format(i),noisy_image.reshape(rows,cols))
        hist_save(noisy_image.reshape(rows,cols), i, "Gaussian", amount_bins)
        
    noisy_image = noisy_image.reshape(rows,cols)
    
    if(histogram_flag == "1"):
        hist_display(noisy_image, amount_bins)

    noisy_image = noisy_image.astype(np.float32)

    return(noisy_image)