from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy,os

os.environ["CC"] = "gcc"

ext_modules = [
    Extension(
        "fast_process",
        ["fast_process.pyx"],
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp'],
        include_dirs=[numpy.get_include()],
    )
]

setup(
    name='process-parallel',
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()],
)