def original(I_hat, I_GS, kernel, kernel_size, m, n, kernel_sum):
    for i in range(n):
        for j in range(m):
            temp = 0
            for k in range(kernel_size):
                for l in range(kernel_size):
                    temp += kernel[k,l] * I_GS[i+k,j+l]
        
            I_hat[i,j] = (temp/kernel_sum)
    return(I_hat)

def slow(float[:,:] I_hat, float[:,:] I_GS, float[:,:] kernel, int kernel_size, int m, int n, int kernel_sum):
    cdef float temp
    cdef int i,j,k,l

    for i in range(n):
        for j in range(m):
            temp = 0
            for k in range(kernel_size):
                for l in range(kernel_size):
                    temp += kernel[k,l] * I_GS[i+k,j+l]
        
            I_hat[i,j] = (temp/kernel_sum)
    return(I_hat)