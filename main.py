import numpy as np
import math, Exceptions
# Custom C Function
import fast_process
import slow_process
# Timing
import timeit, os
from functools import partial
# Custom Python Functions
from functions import padder, gaussian, file_type, display_image
from functions import import_image, import_data, write_image, noise_generator
from kernels import threads_opencl_blur, block_thread_opencl_blur, buffer_opencl_blur
# Arguements
import argparse


# Create arguments for python script to take in
parser = argparse.ArgumentParser(description="Box Blur python script")
# Argument for filename
parser.add_argument("File", help="File to processes")
# Argument for kernel size
parser.add_argument("Kernel", help="Kernel size for the bluring, the bigger the blurrier")
# Argument for setting how many iterations to do for timing test
parser.add_argument("--iterations", type=int, default="0", help="How many iterations to test to calculate averages, all timing is disabled if not set")
# Argument for enabling opencl
parser.add_argument("--opencl", action='store_const', const="1", help="Enable Opencl")
# Argument for which opencl device to utilize
parser.add_argument("--device", type=str, help="Set the Open CL device")
# Argument for debugging in opencl compile
parser.add_argument("--output", default="0", action='store_const', const="1", help="Set PYOPENCL_COMPILER_OUTPUT to 1")
parser.add_argument("--gaussian", action='store_const', const="1", help="Utilize gaussian blur instead of box blur")
parser.add_argument("--timing", action='store_const', const="1", help="Output timing info for opencl calls")
parser.add_argument("--noise", action='store_const', const="1", help="Add random gaussian noise to the image")
parser.add_argument("--histogram", action='store_const', default="0", const="1", help="Add random gaussian noise to the image")
parser.add_argument("--bins", type=int, default=256, help="Amount of bins for historgram")

args = parser.parse_args()

# If opencl device is changed from default then enable opencl flag
if (args.device):
    args.opencl = 1
if (args.bins != 256):
    args.historgram = "1"

# Supported formats for files to take in
# Image and data formats must be different due to the way the 
# data parsing is happening wiht numpy and opencv
image_formats = ["png","jpg","tiff"]
data_formats = ["csv","txt"]

# Original sequencial implementation for box blur
def original(I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum):
    for i in range(rows):
        for j in range(columns):
            temp = 0
            for k in range(kernel_size):
                for l in range(kernel_size):
                    temp += kernel[k,l] * I_GS[i+k,j+l]

            I_hat[i,j] = (temp/kernel_sum)
    return(I_hat)



# Function to determine which functions are being tested and passing the correct arguments in the correct positions
def timerprint(Timing, padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum, ctx=None):
    if (Timing == "Original"):
        times = timeit.Timer(partial(original, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Hybrid C"):
        times = timeit.Timer(partial(slow_process.original, I_hat, I_GS, kernel,
            kernel_size, columns, rows, kernel_sum)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Native C"):
        times = timeit.Timer(partial(slow_process.slow, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Parallel C"):
        times = timeit.Timer(partial(fast_process.fast, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Thread OpenCl"):
        times = timeit.Timer(partial(threads_opencl_blur, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum, args.device, args.output, args.timing, ctx=ctx)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Block Thread OpenCl"):
        times = timeit.Timer(partial(block_thread_opencl_blur, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum, args.device, args.output, args.timing, ctx=ctx)).repeat(repeat=args.iterations,number=1)
    elif (Timing == "Buffer OpenCl"):
        times = timeit.Timer(partial(buffer_opencl_blur, I_hat, I_GS, kernel, kernel_size,
            columns, rows, kernel_sum, args.device, args.output, args.timing, ctx=ctx)).repeat(repeat=args.iterations,number=1)
    else:
        times = [0]

    # Print the average time for the given function that was called
    print(Timing,"\tAverage\t",sum(times)/args.iterations)

# Main function
if __name__ == '__main__':
    # Cast the arguement for kernel size to int and put it in a variable
    kernel_size = int(args.Kernel)
    # Validate that the kernel is an odd size so there is a center
    if(kernel_size % 2 == 0):
        # Exit the script without running anything
        raise Exceptions.InputException("Kernel size is not odd")

    # Calculate the amount of padding that will be needed based on the kernel size
    padding = math.floor(kernel_size/2)

    # Grab the file extension
    extension = file_type(args.File)

    # Check if the file extension is supported
    exist = False
    for format in image_formats:
        if(extension == format):
            exist = True
            I_GS = import_image(args.File)
            break
    for format in data_formats:
        if(extension == format):
            exist = True
            I_GS = import_data(args.File)
            break

    # If the file extension is not supported return an error and exit
    if(exist is False):
        raise Exceptions.InputException("Error invalid file type")

    # Calculate the sizes of the image
    rows=np.shape(I_GS)[0]
    columns=np.shape(I_GS)[1]


    if(args.noise):
        I_GS = noise_generator(I_GS,args.histogram, args.bins)

    # Create a matrix of 0s that will be the same size as the original image to hold blured image
    I_hat = np.zeros((np.shape(I_GS)[0],np.shape(I_GS)[1]),dtype=np.float32)
    # Create matrix of 1s to be utilized for the box blur
    if(args.gaussian):
        kernel = gaussian(kernel_size)
    else:
        kernel = np.ones((kernel_size,kernel_size),dtype=np.float32)

    # Pad the original image
    I_GS = padder(I_GS,padding)
    kernel_sum = np.sum(kernel)

    if(args.iterations != 0):
        # Timing functions, comment out the ones that are not being tested
        #timerprint("Original", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum)
        #timerprint("Hybrid C", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum)
        #timerprint("Native C", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum)
        timerprint("Parallel C", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum)
        if(args.opencl):
            print("Utilizing Opencl")
            import pyopencl as cl
             # If the device flag is used then utilize that device and do not prompt for device selection
            if (args.device):
                os.environ['PYOPENCL_CTX'] = args.device
            os.environ['PYOPENCL_COMPILER_OUTPUT'] = args.output
            ctx = cl.create_some_context()

            timerprint("Thread OpenCl", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum, ctx)
            print("\n")
            timerprint("Block Thread OpenCl", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum, ctx)
            print("\n")
            timerprint("Buffer OpenCl", padding, I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum, ctx)
            print("\n")
    else:
        if (args.opencl):
            print("Utilizing Opencl")
            import pyopencl as cl
             # If the device flag is used then utilize that device and do not prompt for device selection
            if (args.device):
                os.environ['PYOPENCL_CTX'] = args.device
            os.environ['PYOPENCL_COMPILER_OUTPUT'] = args.output
            ctx = cl.create_some_context()
            I_hat = block_thread_opencl_blur(I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum, args.device, args.output, args.timing, ctx)
        else:
            print("Utilizing CPU")
            I_hat = fast_process.fast(I_hat, I_GS, kernel, kernel_size, columns, rows, kernel_sum)

    # Cast the final blur image to int so opencv can create the image from the values
    I_hat = I_hat.astype(np.uint8)
    # Call funtion to create the image file with the (name, data) arguments
    write_image('Blur',I_hat)
