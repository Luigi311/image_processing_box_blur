from cython.parallel import prange
import numpy as np

cimport cython
cimport numpy as np
np.get_include()

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def fast(np.ndarray[DTYPE_t, ndim=2, mode="c"] I_hat, np.ndarray[DTYPE_t, ndim=2, mode="c"] I_GS, 
        np.ndarray[DTYPE_t, ndim=2, mode="c"] kernel, int kernel_size, int m, int n, int kernel_sum):
    
    cdef Py_ssize_t i,j,k,l
    cdef float temp

    for i in prange(n, nogil=True):
        for j in prange(m):
            temp = 0
            for k in prange(kernel_size):
                for l in prange(kernel_size):
                    temp += kernel[k,l] * I_GS[i+k,j+l]
        
            I_hat[i,j] = (temp/kernel_sum)
            
    return(I_hat)

