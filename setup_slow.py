from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules = [
    Extension(
        "slow_process",
        ["slow_process.pyx"],
        extra_compile_args=[],
        extra_link_args=[],
        include_dirs=[],
    )
]

setup(
    name='process',
    ext_modules=cythonize(ext_modules),
    include_dirs=[],
)