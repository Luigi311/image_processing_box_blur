import numpy as np
import pyopencl as cl
import pyopencl.array, time, os

# Functions to test performance with just threads and no blocks specified
def threads_opencl_blur(I_hat, I_GS, kernel, kernel_size, cols, rows, kernel_sum, device=None, output="0", timing=None, ctx=None):
    # If timing flag is used then time the opencl calls for debugging
    if(timing):
        start = time.time()
    
    # Intialize OpenCL and create variables that are required
    queue = cl.CommandQueue(ctx)
    mf = cl.mem_flags
    
    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Setup: ",end-start)

        start = time.time()

    # Flatten all the matrices so they can be used by the opencl kernel
    I_hat = I_hat.ravel()
    I_GS = I_GS.ravel()
    kernel = kernel.ravel()
    
    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Flatten: ",end-start)

        start = time.time()

    # Copy the matrices from the host(CPU) to the device(GPU)
    # Copy over the image and kernel in read only format 
    I_GS_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=I_GS)
    kernel_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=kernel)

    # Copy over the output image matrix in write only format
    I_hat_buf = cl.Buffer(ctx, mf.WRITE_ONLY, I_hat.nbytes)

    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Creating Buffers: ",end-start)

        start = time.time()

    # Creating the opencl kernel to do the blur and compile it
    prg = cl.Program(ctx, """
        __kernel void blur(ushort kernel_sum, ushort total_cols, ushort kernel_size, __global float *I_GS, __global float *F, __global float *I_hat) {
            int gid = get_global_id(0);
            int row = gid/total_cols;
            int col = gid%total_cols;
            int i,j;

            float temp = 0;

            for(i=0;i<kernel_size;i++) {
                for(j=0;j<kernel_size;j++){
                    temp += F[(i*kernel_size) + j] * I_GS[(row + i)*(total_cols + (kernel_size-1)) + (col+j)];
                }
            }
            I_hat[(row*total_cols) + col] = (temp/kernel_sum);
        }
    """).build()
    
    # If timing flag is used then print out the amount of time it took to compile the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Compiling Kernel: ",end-start)

        start = time.time()

    # Call the opencl kernel to do the blur and pass it over the variables being used
    prg.blur(queue, (rows*cols,), None, np.uint16(kernel_sum), np.uint16(cols), 
                                        np.uint16(kernel_size), I_GS_buf, kernel_buf, I_hat_buf)
    
    # If timing flag is used print out the amount of time it took to run the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Running Kernel: ",end-start)

        start = time.time()

    # Copy the output image from the device(GPU) to host(CPU) so it can be returned
    cl.enqueue_copy(queue, I_hat, I_hat_buf)

    # If timing flag is used print out the time it took to copy the image back 
    if(timing):
        queue.finish()
        end = time.time()
        print("Copy back: ",end-start)

    # Reformat the image into an actual matrix instead of a 1d array
    I_hat = I_hat.reshape(rows,cols)

    return(I_hat)

# Functions to test performance with threads and blocks specified
def block_thread_opencl_blur(I_hat, I_GS, kernel, kernel_size, cols, rows, kernel_sum, device=None, output="0", timing=None, ctx=None):
    
    # If timing flag is used then time the opencl calls for debugging
    if(timing):
        start = time.time()

    # Intialize OpenCL and create variables that are required
    queue = cl.CommandQueue(ctx)
    mf = cl.mem_flags
    
    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Setup: ",end-start)

        start = time.time()

    # Flatten all the matrices so they can be used by the opencl kernel
    I_hat = I_hat.ravel()
    I_GS = I_GS.ravel()
    kernel = kernel.ravel()

    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Flatten: ",end-start)

        start = time.time()


    # Copy the matrices from the host(CPU) to the device(GPU)
    # Copy over the image and kernel in read only format 
    I_GS_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=I_GS)
    kernel_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=kernel)

    # Copy over the output image matrix in write only format
    I_hat_buf = cl.Buffer(ctx, mf.WRITE_ONLY, I_hat.nbytes)

    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Creating Buffers: ",end-start)

        start = time.time()

    # Creating the opencl kernel to do the blur and compile it
    prg = cl.Program(ctx, """
        __kernel void blur(ushort kernel_sum, ushort total_cols, ushort kernel_size, __global float *I_GS, __global float *F, __global float *I_hat) {
            int row = get_global_id(0);
            int col = get_global_id(1);

            int i,j;
            float temp = 0;

            for(i=0;i<kernel_size;i++) {
                for(j=0;j<kernel_size;j++){
                    temp += F[(i*kernel_size) + j] * I_GS[(row + i)*(total_cols + (kernel_size-1)) + (col+j)];
                }
            }
            I_hat[(row*total_cols) + col] = (temp/kernel_sum);
        }
    """).build()

    # If timing flag is used then print out the amount of time it took to compile the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Compiling Kernel: ",end-start)

        start = time.time()

    # Call the opencl kernel to do the blur and pass it over the variables being used
    prg.blur(queue, (rows,cols), None, np.uint16(kernel_sum), np.uint16(cols), 
                                        np.uint16(kernel_size), I_GS_buf, kernel_buf, I_hat_buf)
    
    # If timing flag is used print out the amount of time it took to run the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Running Kernel: ",end-start)

        start = time.time()

    # Copy the output image from the device(GPU) to host(CPU) so it can be returned
    cl.enqueue_copy(queue, I_hat, I_hat_buf)

    # If timing flag is used print out the time it took to copy the image back 
    if(timing):
        queue.finish()
        end = time.time()
        print("Copy back: ",end-start)

    # Reformat the image into an actual matrix instead of a 1d array
    I_hat = I_hat.reshape(rows,cols)

    return(I_hat)

# Functions to test performance with a different type of buffer creation
def buffer_opencl_blur(I_hat, I_GS, kernel, kernel_size, cols, rows, kernel_sum, device=None, output="0", timing=None, ctx=None):
    # If timing flag is used then time the opencl calls for debugging
    if(timing):
        start = time.time()

    # Intialize OpenCL and create variables that are required
    queue = cl.CommandQueue(ctx)
    mf = cl.mem_flags
    
    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Setup: ",end-start)

        start = time.time()

    # Flatten all the matrices so they can be used by the opencl kernel
    I_hat = I_hat.ravel()
    I_GS = I_GS.ravel()
    kernel = kernel.ravel()

    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Flatten: ",end-start)

        start = time.time()

    # Copy the matrices from the host(CPU) to the device(GPU)
    I_GS_buf = cl.array.to_device(queue, I_GS)
    kernel_buf = cl.array.to_device(queue, kernel)

    # Copy over the output image matrix in write only format
    I_hat_buf = cl.Buffer(ctx, mf.WRITE_ONLY, I_hat.nbytes)

    # If timing flag is used print out the time for Copying the memory to device
    if(timing):
        queue.finish()
        end = time.time()
        print("Creating Buffers: ",end-start)

        start = time.time()

    # Creating the opencl kernel to do the blur and compile it
    prg = cl.Program(ctx, """
        __kernel void thread_blur(ushort kernel_sum, ushort total_cols, ushort kernel_size, __global float *I_GS, __global float *F, __global float *I_hat) {
            int gid = get_global_id(0);
            int row = gid/total_cols;
            int col = gid%total_cols;
            int i,j;

            float temp = 0;

            for(i=0;i<kernel_size;i++) {
                for(j=0;j<kernel_size;j++){
                    temp += F[(i*kernel_size) + j] * I_GS[(row + i)*(total_cols + (kernel_size-1)) + (col+j)];
                }
            }
            I_hat[(row*total_cols) + col] = (temp/kernel_sum);
        }
        
        __kernel void blocks_blur(ushort kernel_sum, ushort total_cols, ushort kernel_size, __global float *I_GS, __global float *F, __global float *I_hat) {
            int row = get_global_id(0);
            int col = get_global_id(1);
            
            int i,j;
            float temp = 0;

            for(i=0;i<kernel_size;i++) {
                for(j=0;j<kernel_size;j++){
                    temp += F[(i*kernel_size) + j] * I_GS[(row + i)*(total_cols + (kernel_size-1)) + (col+j)];
                }
            }
            I_hat[(row*total_cols) + col] = (temp/kernel_sum);
        }
    """).build()

    # If timing flag is used print out the amount of time it took to run the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Compiling Kernel: ",end-start)

        start = time.time()

    # Upon testing Intel handles operations faster with blocks and threads and Nvidia handles threads only better
    if "Intel" in (str(queue.device)):
        # Call the opencl kernel to do the blur and pass it over the variables being used
        prg.blocks_blur(queue, (rows,cols), None, np.uint16(kernel_sum), np.uint16(cols), np.uint16(kernel_size), 
                                       I_GS_buf.data, kernel_buf.data, I_hat_buf)
    elif "NVIDIA" in (str(queue.device)):
        # Call the opencl kernel to do the blur and pass it over the variables being used
        prg.thread_blur(queue, (rows*cols,), None, np.uint16(kernel_sum), np.uint16(cols), np.uint16(kernel_size), 
                                       I_GS_buf.data, kernel_buf.data, I_hat_buf)
    else:
        # Call the opencl kernel to do the blur and pass it over the variables being used
        prg.thread_blur(queue, (rows*cols,), None, np.uint16(kernel_sum), np.uint16(cols), np.uint16(kernel_size), 
                                       I_GS_buf.data, kernel_buf.data, I_hat_buf)
    
    # If timing flag is used print out the amount of time it took to run the kernel
    if(timing):
        queue.finish()
        end = time.time()
        print("Running Kernel: ",end-start)

        start = time.time()

    # Copy the output image from the device(GPU) to host(CPU) so it can be returned
    cl.enqueue_copy(queue, I_hat, I_hat_buf)
    
    # If timing flag is used print out the time it took to copy the image back 
    if(timing):
        queue.finish()
        end = time.time()
        print("Copy back: ",end-start)

    # Reformat the image into an actual matrix instead of a 1d array
    I_hat = I_hat.reshape(rows,cols)

    # If timing flag is used print out the time it took to copy the image back 
    if(timing):
        queue.finish()
        end = time.time()
        print("Reshape: ",end-start)

    return(I_hat)