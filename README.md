# image_processing_box_blur
  Box blur implementation in python 3 utilizing Cython for fast compute and paralalization. 
  
  Cython scripts must be compiled inorder to generate the C files for the functions being called. The compilations can be done automatically by running the build script. Compilation only happens when changes are made to the cython script or when a compiled version was not build for the system trying to use it.

# Setup
Create a virtualenv or utilize the --user flag on pip  

```bash
$ pip3 install -r requirements.txt  
$ chmod +x build.sh  
$ ./build.sh  
$ python3 main.py <data_file> <kernel_size> <arguments>
```

# Profiling
Profiling can be done with vmprof to create a visualization on what processes are taking the most amount of time in order to troubleshoot what is causing bottlenecks in the computations. Utiliing the offical servers for vmprof should be as simple as calling vmprof as a module and giving it the web flag.
```bash
$ python3 -m vmprof --web --lines kern.py &> /dev/null
```
It is also possible to utilize your own vmprof server by launching one in a docker container, information can be found here https://github.com/vmprof/vmprof-server. Once a server instance is up and running you can utilize the weburl flag to redirect the output to your server instead of the public version. Optionally you can use the timeit.sh script which will run the command and point it to localhost on the default port.
```bash
$ chmod +x timeit.sh
$ ./timeit.sh
```